
const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config()
const app = express();
const port = 4000;
const  mongoose = require('mongoose');
const swaggerJsdoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const productRoute = require('./routes/products')
const transactionRoute = require('./routes/transactions')
const userRoute = require('./routes/user')
const pokemonRoute = require('./routes/pokemon')
const cardRoute = require('./routes/card')
const {APIToolkit} = require("apitoolkit-express");
const sanitize = require('./middleware/sanitizer')
app.use(bodyParser.json());

mongoose
    .connect(process.env.MONGO_URI, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => console.log('BDD connecte'))
    .catch(err => console.log('Erreur :' , err))

const apitoolkitClient = APIToolkit.NewClient({ apiKey: "kPRMeMZMPXozm9wahKZsHzZP9DnER9aetb241L1bpzlb8I2T" });
app.use(apitoolkitClient.expressMiddleware);
app.use(sanitize);

app.use('/product',productRoute)
app.use('/transaction',transactionRoute)
app.use('/user',userRoute)
app.use('/pokemon',pokemonRoute)
app.use('/card',cardRoute)

const options = {
    definition: {
        openapi: "3.1.0",
        info: {
            title: "LogRocket Express API with Swagger",
            version: "0.1.0",
            description:
                "This is a simple CRUD API application made with Express and documented with Swagger",
            license: {
                name: "MIT",
                url: "https://spdx.org/licenses/MIT.html",
            },
            contact: {
                name: "LogRocket",
                url: "https://logrocket.com",
                email: "info@email.com",
            },
        },
        servers: [
            {
                url: "http://localhost:3000",
            },
        ],
    },
    apis: ["./routes/*.js"],
};

const specs = swaggerJsdoc(options);

app.use('/api-docs',swaggerUi.serve,swaggerUi.setup(specs))

app.listen(port, () => {
    console.log('Je marche')
})
