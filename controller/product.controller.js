const Product = require('../model/product.mysql');

exports.create = (req,res) => {
    if(!req.body) {
        res.status(400).send({
            message: 'Veuiller renseigner des données svp'
        })
    }

    const productInsert = new Product({
        title: req.body.title,
        description: req.body.description,
        price: req.body.price,
        img: req.body.img,
    })

    Product.create(productInsert, (err,data) => {
        if(err)
            res.status(500).send({
                message: err.message || 'une erreur est survenu'
            })
        else res.send(data)
    })
}
