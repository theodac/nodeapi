const Transaction = require("../model/transaction.mongo");

exports.create = (req,res) => {
    const transactionField = {
        name: req.body.name,
        price : req.body.price,
        libelle : req.body.libelle
    }

    const newTransaction = new Transaction(transactionField);
    newTransaction.save().then(data => {
        console.log(data)
        res.json(data)
    })
}

exports.findAll = (req,res) => {
    Transaction.find().then(data => {
        res.json(data);
    })
}

exports.findOne = (req,res) => {
    Transaction.findById(req.params.id).then(data => {
        res.json(data);
    })
}
