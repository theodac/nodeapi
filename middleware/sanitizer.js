const sanitizeUserData = (req,res,next) => {
    //Intercepter la methode json pour la nettoyer

    const orginalJson = res.json;
    console.log(orginalJson)

    res.json = function (data) {

        const sanitizeUser = (user) => {
            delete user.password;
            delete user.resetToken;
            delete user.resetTokenExpires;
            user.password = null;
            return user
        }




        if(Array.isArray(data)){
            data = data.map(user => {
                if( user && typeof user === 'object'){
                    return sanitizeUser(user)
                }
                return user
            })
        }


        orginalJson.call(this,data);

    }
    next();
}

module.exports = sanitizeUserData;
