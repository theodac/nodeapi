const jwt = require('jsonwebtoken')
const authenticateJWT = (req,res,next) => {
    const header = req.headers.authorization;

    if(header) {
        const token = header.split(' ')[1];

        jwt.verify(token, process.env.SECRET_KEY,(err,user) => {
            if(err) {
                return res.status(403).json('pas ok');
            }

            req.user = user;
            next();
        })
    }else {
        res.status(401).json('pas ok')
    }
}

module.exports = authenticateJWT;
