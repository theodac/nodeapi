const sql = require('../config/db')

const Product = function (product) {
    this.title = product.title;
    this.description = product.description;
    this.price = product.price;
    this.img = product.img;
    this.created_at = Date.now();
}

Product.create = (newProduct, result) => {
    sql.query('INSERT INTO product SET ?', newProduct, (err,res) => {
        if(err) {
            console.log('j\'ai une erreur');
            result(err,null)
            return
        }
        console.log('produit à etait crée');
        result(null, {id:res.insertId, ...newProduct})
    })
}

Product.findAll = (result) => {
    sql.query('SELECT * FROM product', (err,res) => {
        if(err) {
            console.log('j\'ai une erreur');
            result(err,null)
            return
        }
        console.log('voici vos produit');
        result(null, res)
    })
}


module.exports = Product;
