var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TransactionSchema = new Schema({
    name : {
        type: String,
        required : true,
    },
    price : Number,
    created_at : {
        type: Date,
        default: Date.now()
    },
    libelle : String,

})

module.exports = mongoose.model('Transaction', TransactionSchema)
