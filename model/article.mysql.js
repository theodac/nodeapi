const sql = require('../config/db')

const Article = function (article) {
    this.title = article.title;
    this.description = article.description;
    this.created_at = new Date();
}

Article.create = (newArticle, result) => {
    sql.query('INSERT INTO article SET ?', newArticle, (err,res) => {
        if(err) {
            console.log('j\'ai une erreur');
            result(err,null)
            return
        }
        console.log('produit à etait crée');
        result(null, {id:res.insertId, ...newArticle})
    })
}

Article.findAll = (result) => {
    sql.query('SELECT * FROM article', (err,res) => {
        if(err) {
            console.log('j\'ai une erreur');
            result(err,null)
            return
        }
        console.log('voici vos articles');
        result(null, res)
    })
}


module.exports = Article;
