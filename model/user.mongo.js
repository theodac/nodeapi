var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
var UserSchema = new Schema({
    name : {
        type: String,
        required : true,
    },
    email : {
      type:String,
      match: [/^\S+@\S+\.\S+$/, "Email pas correcte"],
    },
    //pas de doublon en bdd
    username: {type:String, required: true,unique: true},
    password: {type: String,required:true, minLength: [3, "Le mot de passe doit faire 3 caracteres"]},
    transaction : [
        {
            ref: 'Transaction',
            type: mongoose.Schema.Types.ObjectId
        }
    ],
    resetToken:  {type: String, default: null},
    resetTokenExpires:  {type: Date, default: null}

});

UserSchema.pre('save', async function(next) {
    if(this.isModified('password') || this.isNew){
        this.password = await bcrypt.hash(this.password,10);
    }
    next();
});

UserSchema.methods.comparePassword = function (password) {
    return bcrypt.compare(password,this.password);
}

module.exports = mongoose.model('User', UserSchema)
