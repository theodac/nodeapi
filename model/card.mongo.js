var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CardSchema = new Schema({
    pokemon : {
        ref: 'Pokemon',
        type: mongoose.Schema.Types.ObjectId
    },
    description : {
        type: String,
        required : true
    },
    img : {
        type: String
    }
})

module.exports = mongoose.model('Card', CardSchema)
