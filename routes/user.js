/**
 * info:
 *   description: api node
 *   termsOfService: https://apitoolkit.io/terms-and-conditions/
 *   title: nodeQecu
 *   version: 1.0.0
 * openapi: 3.0.0
 * paths:
 *   /user/findAll:
 *     get:
 *       description: ''
 *       requestBody:
 *         content:
 *           application/json:
 *             schema:
 *               properties:
 *                 name:
 *                   description: ''
 *                   example: Appareil
 *                   format: text
 *                   type: string
 *                 transaction:
 *                   description: ''
 *                   example: 6656f8b4c43171a1784d74e1
 *                   format: text
 *                   type: string
 *               type: object
 *       responses:
 *         '200':
 *           content:
 *             application/json:
 *               schema:
 *                 items:
 *                   properties:
 *                     __v:
 *                       description: ''
 *                       example: 0
 *                       format: integer
 *                       type: number
 *                     _id:
 *                       description: ''
 *                       example: 665718a1e48bf21cf2494cb1
 *                       format: text
 *                       type: string
 *                     name:
 *                       description: ''
 *                       example: Appareil
 *                       format: text
 *                       type: string
 *                     transaction:
 *                       items:
 *                         properties:
 *                           __v:
 *                             description: ''
 *                             example: 0
 *                             format: integer
 *                             type: number
 *                           _id:
 *                             description: ''
 *                             example: 6656f8b4c43171a1784d74e1
 *                             format: text
 *                             type: string
 *                           created_at:
 *                             description: ''
 *                             example: '2024-05-29T09:41:36.060Z'
 *                             format: text
 *                             type: string
 *                           libelle:
 *                             description: ''
 *                             example: Canon
 *                             format: text
 *                             type: string
 *                           name:
 *                             description: ''
 *                             example: Appareil
 *                             format: text
 *                             type: string
 *                           price:
 *                             description: ''
 *                             example: 15
 *                             format: integer
 *                             type: number
 *                         type: object
 *                       type: array
 *                   type: object
 *                 type: array
 *           headers:
 *             content:
 *               schema:
 *                 properties:
 *                   content-length:
 *                     description: ''
 *                     example: '286'
 *                     format: integer
 *                     type: string
 *                   content-type:
 *                     description: ''
 *                     example: application/json; charset=utf-8
 *                     format: text
 *                     type: string
 *                   etag:
 *                     description: ''
 *                     example: W/"11e-gpSnXJtp7A58ZAK/4o95FiomUL8"
 *                     format: text
 *                     type: string
 *                   x-powered-by:
 *                     description: ''
 *                     example: Express
 *                     format: text
 *                     type: string
 *                 type: object
 *   /user/postUser:
 *     post:
 *       description: ''
 *       requestBody:
 *         content:
 *           application/json:
 *             schema:
 *               properties:
 *                 name:
 *                   description: ''
 *                   example: Appareil
 *                   format: text
 *                   type: string
 *                 transaction:
 *                   description: ''
 *                   example: 6656f8b4c43171a1784d74e1
 *                   format: text
 *                   type: string
 *               type: object
 *       responses:
 *         '200':
 *           content:
 *             application/json:
 *               schema:
 *                 properties:
 *                   __v:
 *                     description: ''
 *                     example: 0
 *                     format: integer
 *                     type: number
 *                   _id:
 *                     description: ''
 *                     example: 66571dc7ecb8e4fa5d2148c7
 *                     format: text
 *                     type: string
 *                   name:
 *                     description: ''
 *                     example: Appareil
 *                     format: text
 *                     type: string
 *                   transaction:
 *                     description: ''
 *                     example: 6656f8b4c43171a1784d74e1
 *                     format: text
 *                     type: string
 *                 type: object
 *           headers:
 *             content:
 *               schema:
 *                 properties:
 *                   content-length:
 *                     description: ''
 *                     example: '103'
 *                     format: integer
 *                     type: string
 *                   content-type:
 *                     description: ''
 *                     example: application/json; charset=utf-8
 *                     format: text
 *                     type: string
 *                   etag:
 *                     description: ''
 *                     example: W/"67-5FduaiHHhE2AB6tz+g5HunsmY3k"
 *                     format: text
 *                     type: string
 *                   x-powered-by:
 *                     description: ''
 *                     example: Express
 *                     format: text
 *                     type: string
 *                 type: object
 * servers:
 *   - url: localhost
 * @type {e | (() => Express)}
 */
const express = require('express')
const router = express.Router();
require('dotenv').config();
const jwt = require('jsonwebtoken')
const authMiddle  = require('../middleware/jwt')

const SECRET_KEY = process.env.SECRET_KEY
const User = require('../model/user.mongo');
const transcationController = require("../controller/transaction.controller");
const crypto = require('crypto')
const rateLimit = require('express-rate-limit');

const limiter = rateLimit({
    windowMs: 1 * 60 * 1000 ,// 1 min,
    max: 100,
    message: 'Too many request '
})

router.post('/postUser', (req,res) => {
    const userField = {
        name: req.body.name,
        transaction : req.body.transaction,
        username: req.body.username,
        password: req.body.password
    }

    const newUser = new User(userField);
    newUser.save().then(data => {
        console.log(data)
        res.json(data)
    })
})

router.post('/login' , limiter, async (req,res) => {
    const {username,password} = req.body;

    try {
        const user = await User.findOne({username});

        if(!user || !(await user.comparePassword(password))){
            return res.status(401).json({message:"Ca marche po "})

        }

        const token = jwt.sign({username: user.username},
            SECRET_KEY, {expiresIn: '1h'});
        res.json({token})
    }catch (e){
            res.status(500).json({message: "Marche po ", err: e})
    }
})

router.get('/protect' ,authMiddle, (req,res) => {
    res.status(200).json('OK')
})
router.get('/findAll' , (req,res) => {
    User.find().populate('transaction').then(data =>  {
        res.json(data);
    })
})

/*
[
    {
        "_id": "665718a1e48bf21cf2494cb1",
        "name": "Appareil",
        "transaction": [],
        "__v": 0
    },
    {
        "_id": "665718bfe48bf21cf2494cb3",
        "name": "Appareil",
        "transaction": [
            {
                "_id": "6656f8b4c43171a1784d74e1",
                "name": "Appareil",
                "price": 15,
                "created_at": "2024-05-29T09:41:36.060Z",
                "libelle": "Canon",
                "__v": 0
            }
        ],
        "__v": 0
    }
]
 */

router.post('/forgot-password' , async (req,res) => {
    const { username } = req.body;

    try {
        const user = await User.findOne({username});

        if(!user) {
            return res.status(404).json({message: 'Pas trouve'})
        }

        const token = crypto.randomBytes(20).toString('hex');
        user.resetToken = token;
        user.resetTokenExpires = Date.now() + 3600000;

        await user.save();

        const resetUrl = 'http://localhost:4000/user/forgot-password/' + token;

        // nodemailer

        //renvoyer le lien en json

        res.status(200).json(resetUrl);

    }catch (e){
        res.status(500).json('Erreur'  + e)
    }
})

router.post('/forgot-password/:token', async (req,res) => {
    const {token} = req.params;
    const {password} = req.body;

    try{
        const user = await User.findOne({
            resetToken : token,
            resetTokenExpires : { $gt: Date.now()}
        })

        if(!user){
            return res.status(400).json({message:'pas ok '})
        }

        user.password = password;
        user.resetToken = null;
        user.resetTokenExpires = null;

        await user.save();
        res.status(200).json({message : 'Mdp reinitialise'})

    }catch(e) {
        res.status(500).json('pas ok')
    }
})
module.exports = router;
