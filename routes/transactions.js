const express = require('express')
const router = express.Router();

const Transaction = require('../model/transaction.mongo');
const transcationController = require('../controller/transaction.controller')

router.post('/postTransac', transcationController.create)
router.get('/findAll' , transcationController.findAll)
router.get('/find/:id' , transcationController.findOne)

module.exports = router;
