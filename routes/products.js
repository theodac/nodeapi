const express = require('express')
const router = express.Router();

const productController = require('../controller/product.controller')

router.post('/postProduct',productController.create )

module.exports = router;
