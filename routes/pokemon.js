const express = require('express')
const router = express.Router();

const Pokemon = require('../model/pokemon.mongo')


router.post('/postPokemon',(req,res) => {
    let obj = {
        name : req.body.pokemon
    }

    const newPokemon = new Pokemon(obj);

    newPokemon.save().then(data => {
        res.json(data);
    }).catch(e => {
        res.status(500).json({erreur: e})
    })
} )

module.exports = router;
