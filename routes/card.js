const express = require('express')
const router = express.Router();
const Card = require('../model/card.mongo')
const multer  = require('multer')
const upload = multer({ storage: multer.diskStorage({
        destination: function (request,file,callback) {
            callback(null,'./uploads')
        }
        ,
        filename(req, file, callback) {
            var temp_file_arr = file.originalname.split('.')

            var temp_file_name = temp_file_arr[0];

            var temp_file_extension = temp_file_arr[1];

            callback(null, temp_file_name + '-'  + Date.now()  +'.' + temp_file_extension);
        }
    })})

router.post('/postCard',upload.single('img'),(req,res) => {
    let obj = {
        pokemon: req.body.pokemon,
        description : req.body.description,
        img: req.file.path
    }
    console.log(req.file,req.body);
    const newCard = new Card(obj)

    newCard.save().then(data => {
        res.json(data)
    }).catch(e => {
        res.status(500).json({erreur : e})
    })
} )

module.exports = router;
